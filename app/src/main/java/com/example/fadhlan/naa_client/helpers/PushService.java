package com.example.fadhlan.naa_client.helpers;

import android.os.AsyncTask;

import com.example.fadhlan.naa_client.models.Message;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

public class PushService extends AsyncTask <String, Void, Void > {

    /**
     * Replace SERVER_KEY with your SERVER_KEY generated from FCM
     * Replace DEVICE_TOKEN with your DEVICE_TOKEN
     */
    private static String SERVER_KEY = "key=AAAAbroC_ds:APA91bGsNms8pECjNt0Mo-XeoM8VpymfpgKTsMno3tUcF8y1kg9m5I-7Vgh4MR95ae3wkDZH0TJu_9ru2_kFrgBRYYxBz-MF-b7cRquFmO5qe8OKVDV8hjz9Gfqqr48YbEJsvxFW7tl2";

    // This is the JSON body of the post
    Message message;
//    String sentKey;
//    String inboxKey;
    // This is a constructor that allows you to pass in the JSON body
    public PushService(Message message) {
        if (message != null) {
            this.message = message;
//            this.sentKey = sentKey;
//            this.inboxKey = inboxKey;
        }
    }


    @Override
    protected Void doInBackground(String... params) {

        HttpsURLConnection connection = null;
        try {
            String BaseURL = "https://fcm.googleapis.com/fcm/send";
            URL url = new URL(BaseURL);
            connection = (HttpsURLConnection) url.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("Authorization", SERVER_KEY);
            JSONObject root = new JSONObject();
            JSONObject notification = new JSONObject();
            JSONObject object = new JSONObject();;
            List<String> ids = new ArrayList<>();
            ids.add(this.message.getReceiver().getToken());

            object.put("messageId", this.message.getMessageId());
            //notification.put("title",name);
            notification.put("body", this.message.getBody());
            notification.put("click_action","https:localhost:4200");
            notification.put("title", "Notification");

            root.put("data", object);
            root.put("notification", notification);
            root.put("to", this.message.getReceiver().getToken().toString());

            byte[] outputBytes = root.toString().getBytes("UTF-8");
            OutputStream os = connection.getOutputStream();
            os.write(outputBytes);
            os.flush();
            os.close();
            connection.getInputStream(); //do not remove this line. request will not work without it gg

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) connection.disconnect();
        }
        return null;
    }


    /**
     * Sends notification to mobile
     */
//    protected void doInBackground(Message message) throws Exception {
//        String pushMessage = "{notification: {" +
//            "title : 'Notification'," +
//            "body :'" + message.getData() + "',"
//            + "click_action: 'http://localhost:4200/#/home}',"
//            + "to:'" + message.getTo().getToken() + "'}";
//
//        // Create connection to send FCM Message request.
//        URL url = new URL("https://fcm.googleapis.com/fcm/send");
//        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//        conn.setRequestProperty("Authorization", "key=" + SERVER_KEY);
//        conn.setRequestProperty("Content-Type", "application/json");
//        conn.setRequestMethod("POST");
//        conn.setDoOutput(true);
//
//        // Send FCM message content.
//        OutputStream outputStream = conn.getOutputStream();
//        outputStream.write(pushMessage.getBytes());
//
//        System.out.println(conn.getResponseCode());
//        System.out.println(conn.getResponseMessage());
//    }
}