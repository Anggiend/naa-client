package com.example.fadhlan.naa_client.helpers;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.fadhlan.naa_client.R;
import com.example.fadhlan.naa_client.models.Message;

import java.util.ArrayList;
import java.util.List;

public class AdapterMessage extends RecyclerView.Adapter<AdapterMessage.MyViewHolder> {
    private List<Message> messageList = new ArrayList<>();

    public AdapterMessage(List<Message> messageList) {
        this.messageList = messageList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.message, parent, false);
        return new MyViewHolder(view);
     }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tv_body.setText(messageList.get(position).getBody().toString());
        holder.tv_sent.setText(messageList.get(position).getSentDate().toString());
        holder.tv_received.setText(messageList.get(position).getReceiveDate().toString());
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_body, tv_sent, tv_received;
        public MyViewHolder(View itemView) {
            super(itemView);
            tv_body = itemView.findViewById(R.id.tv_body);
            tv_sent = itemView.findViewById(R.id.tv_sent);
            tv_received = itemView.findViewById(R.id.tv_received);
        }
    }
}
