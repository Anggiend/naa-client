package com.example.fadhlan.naa_client.helpers;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";
    private String refreshedToken;

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        this.refreshedToken = FirebaseInstanceId.getInstance().getToken();
    }

    public String getRefreshedToken() {
        return this.refreshedToken;
    }
}

