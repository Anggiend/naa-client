package com.example.fadhlan.naa_client.helpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.fadhlan.naa_client.models.Device;


public class HelperDB extends  SQLiteOpenHelper{

    private static final int DATABASE_VERSION = 1;
    // Database Name
    private static final String DATABASE_NAME = "PushNotif";
    // Contacts table name
    private static final String TABLE_USER = "users";
    // Shops Table Columns names
    private static final String KEY_ID = "id";
    private static final String KEY_TOKEN = "token";
    private static final String[] COLUMNS = { KEY_ID, KEY_TOKEN};

    public HelperDB(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_USER + " ("
        + KEY_ID +" INTEGER PRIMARY KEY," + KEY_TOKEN + " TEXT)";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
        onCreate(db);
    }

    public void setUser(Device user){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(KEY_ID, user.getId());
        values.put(KEY_TOKEN, user.getToken());

        // insert
        long result = db.insert(TABLE_USER,null, values);
        System.out.println("result" + result);
        db.close();
    }

    public Device getUserbyToken(String token){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_USER, // a. table
                COLUMNS,// , // b. column names
                " token = ?", // c. selections
                new String[] { String.valueOf(token) }, // d. selections args
                null, // e. group by
                null, // f. having
                null, // g. order by
                null); // h. limit
        Device user = new Device();

        if (cursor != null && cursor.getCount() > 0 && cursor.moveToFirst()) {
            user.setId(Integer.parseInt(cursor.getString(0)));
            user.setToken(cursor.getString(1));
        }

        return user;
    }
}
